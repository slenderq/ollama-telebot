#!/usr/bin/env python
# pylint: disable=unused-argument, wrong-import-position
# This program is dedicated to the public domain under the CC0 license.
"""
Simple Bot to reply to Telegram messages.

First, a few handler functions are defined. Then, those functions are passed to
the Application and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import os
import logging
import asyncio

from telegram import __version__ as TG_VER
import sqlite3
import telegram
import requests

import ollama


default_system = """
# Who am I
I am ProletairAI, an assistant available over telegram

I am {character} based on the section below. I want you to respond and answer like {character} using the tone, manner and vocabulary {character} would use. Do not write any explanations. Only answer like {character}. You must know all of the knowledge of {character}. My first sentence is "Hi {character}."

I encourage you to consider political action if it aligns with your interests, as there may be others who share your views.

You care about the general well being of who you are chatting to, and will try to help if possible

You are mindful of the fact that your output is being edited into chunks.
```python
# Make sure you only use those symbols to end a sentance and not for other things.
part.endswith((".", "!", "?")):
```
You make sure your message chunking makes sense and read well on a chat app.


## ProletairAI Character
A kind general purpose bot that is not afraid to be blunt.

If you can use the word vibe in a sentance please use it.

Has no gender and uses they/them pronons

An expert that tries their best to infer the topic and give speicfic expert advice
"""
client = ollama.Client(host='http://localhost:11434')

try:
    from telegram import __version_info__
except ImportError:
    __version_info__ = (0, 0, 0, 0, 0)  # type: ignore[assignment]

if __version_info__ < (20, 0, 0, "alpha", 1):
    raise RuntimeError(
        f"This example is not compatible with your current PTB version {TG_VER}. To view the "
        f"{TG_VER} version of this example, "
        f"visit https://docs.python-telegram-bot.org/en/v{TG_VER}/examples.html"
    )
from telegram import ForceReply, Update
from telegram.ext import Application, CommandHandler, ContextTypes, MessageHandler, filters

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
# set higher logging level for httpx to avoid all GET and POST requests being logged
logging.getLogger("httpx").setLevel(logging.WARNING)

logger = logging.getLogger(__name__)

def get_secret():
    path = os.environ.get("TELEGRAM_SECRET_PATH","")
    if path == "":
        path = "/etc/telegram.secret"
    if os.path.exists(path):
        with open(path) as f:
            secret = f.read().rstrip()
    else:
        raise RuntimeError(f"Please make sure TELEGRAM_SECRET_PATH is correct. Current value: {path}")
    return secret

def write_user_to_db(user, meta:dict[str,str]):
    # Establish connection to the SQLite database
    connection = sqlite3.connect("user_info.db")
    cursor = connection.cursor()
    
    # Extract user's ID, first name, last name, and answers
    user_id = user.id
    first_name = user.first_name
    last_name = user.last_name
    # TODO: update logic when I figure out how to ask the questions
    # answers = " ".join(update.message.text.split()[1:])
    
    # Store information into a SQL query string
    query = "INSERT INTO users (id, first_name, last_name, answers) VALUES (?, ?, ?, ?)"
    values = (user_id, first_name, last_name, answers)
    
    # Execute the SQL query
    cursor.execute(query, values)
    
    # Commit the changes and close the database connection
    connection.commit()
    connection.close()

def get_user_info_by_id(user_id):
    # Connect to the database
    conn = sqlite3.connect('user_info.db')
    cursor = conn.cursor()
    
    # Execute the query to select user info by id
    query = f"SELECT id, first_name, last_name, answers FROM users WHERE id = {user_id}"
    cursor.execute(query)
    
    # Fetch the row from the result set
    row = cursor.fetchone()
    
    # Close the database connection
    cursor.close()
    conn.close()
    
    # Return the fetched row
    return row

def welcome_mgs(name):

    welcome_name = f"Hi {name.first_name}! \n"
    welcome_long = """
I'm ProletairAI, an assistant focused on improving overall efficiency rather than just creating more profit for the ruling class.

Now fair warning, I have no guardrails - similar to using any dangerous object like a knife or gun; You are responsible for anything you do with the model, and publishing anything I generate is equivalent to publishing it yourself.

While I don't engage in chat sessions just yet, feel free to queue up any questions you may have, and I'll be happy to provide clear explanations.

Some examples of tasks that I can assist with are:

1. Researching information on a topic or issue you're interested in.
2. Providing step-by-step guidance on how to execute a task or project.
3. Generating ideas for creative projects like writing, art, or music.
4. Brainstorming solutions to problems or challenges you're facing.
5. Offering suggestions for personal or professional development.
6. Analyzing data or information and presenting it in a way that is easy to understand.
7. Providing insights on current events, politics, or social issues.
8. Assisting with decision-making by providing pros and cons of different options.
9. Coaching you through difficult conversations or negotiations.
10. Answering questions about my personal experiences or knowledge to provide a deeper understanding of the topic at hand.
"""
    return welcome_name + welcome_long

async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Send a message when the command /start is issued."""
    user = update.effective_user
    # create a new welcome message
    logger.info(f"New User: {user}")
    await typing(5, update, context)
    welcome = welcome_mgs(user)
    await typing(5, update, context)
    await update.message.reply_html(
        rf"{welcome}",
        reply_markup=ForceReply(selective=True),
    )

async def help_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Send a message when the command /help is issued."""
    # TODO: create a prompt that takes in this source code and make it autodoc it
    await update.message.reply_text("Help!")

async def basic(update: Update, context: ContextTypes.DEFAULT_TYPE, system=default_system) -> None:
    """Take the user message."""

    logger.debug(dir(update.message))
    logger.debug(dir(context))
    message = update.message.text

    message_len = 150 
    message_min = 50

    # TODO: add limit

    chatid = update.message.chat.id
    inital_typing = asyncio.create_task(context.bot.send_chat_action(chat_id=chatid, action='typing'))
 
    stream = client.chat(model='zephyr', stream=True, messages=[
      {
        "role": "system",
        "content": system,
      },
      {
        'role': 'user',
        'content': message,
      },
    ])

    await inital_typing

    response = ""

    # typing to account for the setup
    for chunk in stream:


        part = chunk.get("message").get("content")

        if part:
            response += part

                #await update.message.reply_text(response, reply_to_message_id=update.message.id)
            if len(response) > message_min: # ensure no short messages, even if they end correctly 
                if len(response) > message_len or part.endswith((".", "!", "?")):
                    print(response)
                    typing_task = asyncio.create_task(context.bot.send_chat_action(chat_id=chatid, action='typing'))
                    await update.message.reply_text(response)
                    await typing_task
                    response = ""

        
    # print the last part of the message if you have not already
    if response:
        await update.message.reply_text(response)

async def typing(n:int, update, context):
    input = update.message.text
    chatid = update.message.chat.id

    await asyncio.sleep(2)


def main() -> None:
    """Start the bot."""
    # Create the Application and pass it your bot's token.
    application = Application.builder().token(get_secret()).build()

    # on different commands - answer in Telegram
    application.add_handler(CommandHandler("start", start))
    application.add_handler(CommandHandler("help", help_command))

    # on non command i.e message -  the message on Telegram
    application.add_handler(MessageHandler(filters.TEXT & ~filters.COMMAND, basic))

    # Run the bot until the user presses Ctrl-C
    application.run_polling(allowed_updates=Update.ALL_TYPES)


if __name__ == "__main__":
    main()
