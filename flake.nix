{
  description = "ollama-telebot package managed with poetry2nix";

  inputs = {
    nixpkgs     = { url = "github:nixos/nixpkgs/nixpkgs-unstable"; };
    flake-utils = { url = "github:numtide/flake-utils"; };
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix, ... }:
  flake-utils.lib.eachDefaultSystem (system: 
    let
      pkgs = nixpkgs.legacyPackages.${system};
      inherit (poetry2nix.lib.mkPoetry2Nix { inherit pkgs; }) mkPoetryApplication mkPoetryEnv defaultPoetryOverrides;
      python = pkgs.python311Full;
      pythonPkgs = pkgs.python311Packages;
      appEnv = pkgs.poetry2nix.mkPoetryEnv {
        projectDir = self;  
        editablePackageSources = {
          ollama-telebot = ./src;
        };
      };
    in {
      packages = {
        # https://ryantm.github.io/nixpkgs/languages-frameworks/python/#buildpythonpackage-function
        # https://github.com/nix-community/poetry2nix/blob/master/docs/edgecases.md
        ollama-telebot = mkPoetryApplication {
          projectDir = self;
          overrides = defaultPoetryOverrides.extend
          (self: super: {
            ollama = super.ollama.overridePythonAttrs
            (
              old: {
                buildInputs = (old.buildInputs or [ ]) ++ [ super.poetry ];
              }
            );
          });

        };
        default = self.packages.${system}.ollama-telebot;
      };

      devShells = pkgs.mkShell {
          inputsFrom = [ self.packages.${system}.ollama-telebot ];
          buildInputs =  [ pkgs.poetry appEnv ];
        };

      nixosModules.ollama-telebot-service = import ./service.nix;

    });
}
