{ config, lib, pkgs, ... }: with lib; let
  cfg = config.services.ollama-telebot;
in
{
  options.services.ollama-telebot = with lib; {
    enable = mkEnableOption "ollama-telebot service";
  };

  config = mkIf cfg.enable {
    systemd.user.services.telebot = {
      enable = true;
      description = "Ollama Telebot Service";
      serviceConfig = {
        ExecStart = "ollama-telebot";
        Restart = "on-failure";
      };
      wantedBy = [ "graphical-session.target" ];
    };
  };

}
